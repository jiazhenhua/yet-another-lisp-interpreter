(+ 2 3)  ;简单计算

;基本函数
(define (inc x)(+ x 1))
(inc 4)

;递归函数
(define (fact x)
  (cond
    ((= x 1)1)
    (else (* x (fact (- x 1))))))
(fact 5)

;Y组合子
(((lambda (target)
        ((lambda (f) (f f))
         (lambda (f)
           (target
            (lambda(x) ((f f) x))))))
    (lambda (fact)
     (lambda (x)
      (cond
       ((= x 1) 1)
       (else (* x (fact (- x 1)))))))) 5)
