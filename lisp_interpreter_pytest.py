from lisp_interpreter import *

# \testcases\test_demo1.py
def test_parse():
    code1 = '(+ 2 (+ 3 4))'
    code2 = '(lambda (x) (+ x 1))'
    res1 = parse(code1)
    res2 = parse(code2)
    print('\n', res1, '\n', res2)
    assert res1 == [['+', 2, ['+', 3, 4]]]
    assert res2 == [['lambda', ['x'], ['+', 'x', 1]]]

def test_eval_apply():
    code1 = '(+ 2 (+ 3 4)) (* 2 (+ 2 3))'
    res1 = eval(parse(code1)[0],{})
    res2 = eval(parse(code1)[1],{})
    print('\n%d\n%d'% (res1, res2))
    assert res1 == 9
    assert res2 == 10
    run(code1)
    code2 = '(** 2 3)'
    run(code2)

def test_env():
    # code1 = '(+ 2 (+ 3 a)) (* 2 (+ 2 b))'   #{'a':1, 'b':2, 'c':3}
    # res1 = eval(parse(code1)[0], global_env)
    # res2 = eval(parse(code1)[1], global_env)
    # print('\n%d\n%d'% (res1, res2))
    # assert res1 == 6
    # assert res2 == 8
    print()
    code2 = '(define d 8) (* 2 (+ 2 d))' #{'a':1, 'b':2, 'c':3, 'd':4}
    run(code2)

def test_apply_lambda():
    print()
    code1 = '((lambda (x) (+ x 1))5)'
    code2 = '''
    (define fact
     (lambda (x)
      (cond
       ((= x 1) 1)
       (else (* x (fact (- x 1)))))))
    (fact 5)
    '''
    res1 = eval(parse(code1)[0], global_env)
    for i in parse(code2):
        res2 = eval(i, global_env)
    print('%d\n%d' % (res1, res2))
    assert res1 == 6
    assert res2 == 120

    # Y组合子
    code3 = '''
    (((lambda (target)
        ((lambda (f) (f f))
         (lambda (f)
           (target
            (lambda(x) ((f f) x))))))
    (lambda (fact)
     (lambda (x)
      (cond
       ((= x 1) 1)
       (else (* x (fact (- x 1)))))))) 5)
    '''
    run(code3)
